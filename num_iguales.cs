using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp24
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("inserte el primer numero");
            int num1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("inserte el segundo numero");
            int num2 = Convert.ToInt32(Console.ReadLine());

            if(num1 == num2)
            {
                Console.WriteLine("los numeros son iguales");
            }
            else if (num1 > num2)
            {
                Console.WriteLine("el numero uno es mayor que el numero dos");
            }
            else if (num2 > num1)
            {
                Console.WriteLine("el numero dos es mayor que el numero uno");
            }
            Console.Read();
        }
    }
}
