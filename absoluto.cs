using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace valorabs
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("inserte un numero");
            int num = Convert.ToInt32(Console.ReadLine());
            
            if (num < 0)
            {
                Console.WriteLine("el valor absoluto del numero es el siguiente "+ Math.Abs(num));
            }

            else
            {
                Console.WriteLine("el numero ya es positivo");
            }
            Console.Read();
        }
    }
}
